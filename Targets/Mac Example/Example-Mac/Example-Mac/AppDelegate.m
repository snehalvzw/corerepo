/*
Copyright © 2012-2013 George Cox

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import "AppDelegate.h"
#import "Todo.h"

@implementation AppDelegate

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	[CoreRepo configure:[[[[CoreRepoConfiguration configuration]
						 withStoreType:CoreRepoStoreTypeSQLite]
						 withModelName:@"MacExampleModel" andExtension:@"todos"]
						 debug:YES]];
	
	// Insert a new todo on a background thread
	[CoreRepo usingRepository:^(Repository *repository) {
		NSUInteger count = [[repository allOf:[Todo class]] count];
		Todo *newTodo = [repository createEntity:[Todo class]];
		newTodo.summary = [NSString stringWithFormat:@"Todo #%ld", count + 1];
		newTodo.done = NO;
		[repository save];
	}];
}

@end
