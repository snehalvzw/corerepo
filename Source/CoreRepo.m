/*
Copyright © 2012-2013 George Cox

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import "CoreRepoStack.h"
#import "Repository.h"
#import "CoreRepo.h"

@implementation CoreRepo

+(void)configure:(CoreRepoConfiguration *)configuration {
	[CoreRepo coreDataStackWithConfiguration:configuration];
}

+(CoreRepoStack *)coreDataStackWithConfiguration:(CoreRepoConfiguration *)configuration {
	static CoreRepoStack *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedInstance = [[CoreRepoStack alloc] initWithConfiguration:configuration];
	});
	return sharedInstance;
}

+(CoreRepoStack *)stack {
	return [CoreRepo coreDataStackWithConfiguration:nil];
}

+(void)usingRepository:(void (^)(id))block {
	static Repository *mainThreadRepository = nil;

	__block NSManagedObjectContext *context = [NSThread isMainThread]
	    ? [[CoreRepo stack] uiContext]
	    : [[CoreRepo stack] workerContext];

	Class repoClass = [CoreRepo stack].configuration.repositoryClass;
	Repository *repo = nil;
	if ([NSThread isMainThread]) {
		if (mainThreadRepository == nil)
			mainThreadRepository = [[repoClass alloc] initWithContext:context];

		repo = mainThreadRepository;
	} else
		repo = [[repoClass alloc] initWithContext:context];
    
    block(repo);
}

+(BOOL)isWorkerContext:(NSManagedObjectContext *)context {
	return (context != [[CoreRepo stack] uiContext] && context != [[CoreRepo stack] writerContext]);
}

@end