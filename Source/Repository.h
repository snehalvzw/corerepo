/*
Copyright © 2012-2013 George Cox

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import <CoreData/CoreData.h>
#import "CoreRepoStack.h"

@class NSManagedObjectContext;

@interface Repository : NSObject

@property (nonatomic, strong) NSManagedObjectContext *context;

-(id)initWithContext:(NSManagedObjectContext *)context;

-(NSPredicate *)amendPredicate:(NSPredicate *)predicate forEntityType:(Class)entityClass;

-(id)get:(id)objectId;
-(void)save;
-(void)deleteObject:(NSManagedObject *)object;
-(void)deleteObjectThenSave:(NSManagedObject *)object;

-(NSManagedObjectID *)objectIDFromString:(NSString *)objectIDString;

-(id)createEntity:(Class)class;
-(id)createDetachedEntity:(Class)class;
-(NSFetchRequest *)fetchRequestFor:(Class)class;

-(NSArray *)allOf:(Class)class;
-(NSArray *)allOf:(Class)class withPredicate:(NSPredicate *)predicate;
-(NSArray *)allOf:(Class)class withPredicate:(NSPredicate *)predicate orderedBy:(NSArray *)sortDescriptors;

-(id)singleOrNilOf:(Class)class withPredicate:(NSPredicate *)predicate;
-(id)firstOrNilOf:(Class)class withPredicate:(NSPredicate *)predicate;

-(id)top:(NSUInteger)topN of:(Class)class withPredicate:(NSPredicate *)predicate orderedBy:(NSArray *)sortDescriptors;

-(NSUInteger)count:(Class)class;
-(NSUInteger)count:(Class)class withPredicate:(NSPredicate *)predicate;

@end