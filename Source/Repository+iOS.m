/*
Copyright © 2012-2013 George Cox

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import "Repository+iOS.h"

@implementation Repository (iOS)

-(NSFetchedResultsController *)resultsControllerForAllOf:(Class)class {
	NSFetchRequest *request = [self fetchRequestFor:class];
	return [[NSFetchedResultsController alloc]
											  initWithFetchRequest:request
											  managedObjectContext:self.context
											  sectionNameKeyPath:nil
											  cacheName:nil];
}

-(NSFetchedResultsController *)resultsControllerForAllOf:(Class)class withPredicate:(NSPredicate *)predicate {
	return [self resultsControllerForAllOf:class withFetchRequestSetupBlock:^(NSFetchRequest *request) {
		request.predicate = predicate;
	}];
}

-(NSFetchedResultsController *)resultsControllerForAllOf:(Class)class withPredicate:(NSPredicate *)predicate orderedBy:(NSArray *)sortDescriptors {
	return [self resultsControllerForAllOf:class withFetchRequestSetupBlock:^(NSFetchRequest *request) {
		request.predicate = predicate;
		request.sortDescriptors = sortDescriptors;
	}];
}

-(NSFetchedResultsController *)resultsControllerForAllOf:(Class)class
							  withFetchRequestSetupBlock:(void (^)(NSFetchRequest *))fetchRequestSetupBlock {
	NSFetchRequest *request = [self fetchRequestFor:class];
	
	if (fetchRequestSetupBlock)
		fetchRequestSetupBlock(request);

	return [[NSFetchedResultsController alloc]
											  initWithFetchRequest:request
											  managedObjectContext:self.context
											  sectionNameKeyPath:nil
											  cacheName:nil];
}

@end