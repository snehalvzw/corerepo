/*
Copyright © 2012-2013 George Cox

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import <CoreData/CoreData.h>
#import "CoreRepoStack.h"
#import "CoreRepoConfiguration.h"

@implementation CoreRepoStack {
	NSManagedObjectContext *_uiContext;
	NSManagedObjectContext *_writerContext;
}

-(id)initWithConfiguration:(CoreRepoConfiguration *)configuration {
	if ((self = [super init])) {
		_configuration = configuration;
		[self initializeStaticContexts];
	}
	return self;
}

-(void)initializeStaticContexts {
	_writerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	_writerContext.persistentStoreCoordinator = [self coordinator];

	_uiContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	_uiContext.parentContext = _writerContext;
}

-(NSManagedObjectContext *)uiContext {
	return _uiContext;
}

-(NSManagedObjectContext *)writerContext {
	return _writerContext;
}

-(NSManagedObjectContext *)workerContext {
	NSManagedObjectContext *_workerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	_workerContext.parentContext = _uiContext;
	return _workerContext;
}

-(NSManagedObjectID *)managedObjectIDFromStringRepresentation:(NSString *)stringRepresentation {
	if (stringRepresentation == nil || ![stringRepresentation isKindOfClass:NSString.class])
		return nil;
	return [[self coordinator] managedObjectIDForURIRepresentation:[NSURL URLWithString:stringRepresentation]];
}

-(void)logDebugInfo:(NSString *)format, ... {
	if (!_configuration.debug)
		return;
	va_list args;
	va_start(args, format);
	NSLogv([NSString stringWithFormat:@"CoreRepo: %@", format], args);
	va_end(args);
}

#pragma mark - Stack Setup

-(NSManagedObjectModel *)model {
	static dispatch_once_t onceToken;
	static NSManagedObjectModel *sharedInstance;
	dispatch_once(&onceToken, ^{
		NSURL *modelURL = [[NSBundle mainBundle] URLForResource:_configuration.modelName withExtension:@"momd"];
		sharedInstance = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	});
	return sharedInstance;
}

-(NSPersistentStoreCoordinator *)coordinator {
	static NSPersistentStoreCoordinator *coordinatorInstance;
	static dispatch_once_t onceToken;

	dispatch_once(&onceToken, ^{
		NSManagedObjectModel *mom = [self model];
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSError *error = nil;
		NSDictionary *properties = [_configuration.storeLocation resourceValuesForKeys:[NSArray arrayWithObject:NSURLIsDirectoryKey] error:&error];
		if (!properties) {
			BOOL storeLocationCreated = NO;
			if ([error code] == NSFileReadNoSuchFileError)
				storeLocationCreated = [fileManager createDirectoryAtPath:[_configuration.storeLocation path] withIntermediateDirectories:YES attributes:nil error:&error];
			if (!storeLocationCreated) {
				[self logDebugInfo:@"\nCould not create core data store location:\n%@", error];
				return;
			}
		} else {
			if (![[properties objectForKey:NSURLIsDirectoryKey] boolValue]) {
				NSString *failureDescription = [NSString stringWithFormat:@"Expected a folder to store application data, found a file (%@).", [_configuration.storeLocation path]];
				NSMutableDictionary *dict = [NSMutableDictionary dictionary];
				[dict setValue:failureDescription forKey:NSLocalizedDescriptionKey];
				error = [NSError errorWithDomain:@"CoreRepo_Error_Domain" code:101 userInfo:dict];
				[self logDebugInfo:@"\nError creating persistent store coordinator!\n%@", error];
				return;
			}
		}

		NSURL *url = [_configuration.storeLocation URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", _configuration.modelName, _configuration.modelExtension]];
		NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
		coordinatorInstance = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
		if (![coordinatorInstance addPersistentStoreWithType:[_configuration coreDataStoreType] configuration:nil URL:url options:options error:&error]) {
			[self logDebugInfo:@"Error creating shared store coordinator for %@: %@", [_configuration coreDataStoreType], error];
			return;
		}
	});
	return coordinatorInstance;
}

@end

@implementation NSManagedObjectID (ObjectIDImprovements)
-(NSString *)stringRepresentation {
	return [[self URIRepresentation] absoluteString];
}
@end

@implementation NSManagedObject (ObjectIDImprovements)
-(NSString *)objectIDString {
	return [[self objectID] stringRepresentation];
}
@end