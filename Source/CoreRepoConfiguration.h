/*
Copyright © 2012-2013 George Cox

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

typedef enum {
	CoreRepoStoreTypeSQLite = 0,
	CoreRepoStoreTypeMemory = 1
} CoreRepoStoreType;

@interface CoreRepoConfiguration : NSObject

@property (nonatomic, copy) NSURL *storeLocation;
@property (nonatomic, copy) NSString *modelName;
@property (nonatomic, copy) NSString *modelExtension;
@property (nonatomic) CoreRepoStoreType storeType;
@property (nonatomic) BOOL debug;
@property (nonatomic) Class repositoryClass;
@property (nonatomic, copy) NSPredicate *(^predicateAmendingBlock)(NSPredicate *, Class entityType);

+(instancetype)configuration;
-(instancetype)withStoreLocation:(NSURL *)storeLocation;
-(instancetype)withModelName:(NSString *)modelName andExtension:(NSString *)modelExtension;
-(instancetype)withStoreType:(CoreRepoStoreType)storeType;
-(instancetype)debug:(BOOL)debug;
-(instancetype)repositoryClass:(Class)repositoryClass;
-(instancetype)predicateAmendingBlock:(NSPredicate *(^)(NSPredicate *, Class entityType))predicateAmendingBlock;

-(NSString *)coreDataStoreType;

@end